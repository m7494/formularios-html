/*
 *  Fecha:
 *  
 *  Autor:
 *  
 *  Descripción: Estado de conexión
 */
#include "WiFi.h"
#include "WebServer.h"


const char * ssid="hercab2k20";
const char * passwd="AxVa9295";

WebServer server(80);  // Crear servidor que escucha peticiones por el puerto 80

String pagina="<!DOCTYPE html>"
              "<html>"
              "<head>"
              "<title>ESP32 4</title>"
              "<meta http-equiv='refresh' content='3'>"
              "</head>"
              "<body>"
              "<center><h1>Sensor de Campo Magnetico (Efecto Hall) ESP32</h1>"
              "<hr/>";
     
String media="<p><h3>Valor=";
String fin="</h3></p>"
              "</center>"
              "</body>"
              "</html>";


String pag="";

void localIP(){
  Serial.print("IP asignada:");
  Serial.println(WiFi.localIP());
}

void wifiInit(){
    int intentos =0;
    // Establece modo de operación Estación y desconecta el modulo WiFi si estuviera conectado
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    // Conexión a red específica.
    WiFi.begin(ssid,passwd);
    Serial.print("Conectando .");
    while(WiFi.status() != WL_CONNECTED && intentos++!=30){
      Serial.print(".");
      delay(1000);
    }
    Serial.println();
    if(WiFi.status()== WL_CONNECTED){
        Serial.println("Conexión completada correctamente!!");
        localIP();
    }
    else{
        Serial.print("Tiempo sobrepasado para conectarse a:");
        Serial.println(ssid);
    }
    
}

void leerCampoMagnetico(){
     int medicion = hallRead();
     Serial.println(medicion);
     pag=pagina+media+medicion+fin;
}

void gestorRaiz(){
      leerCampoMagnetico();
      server.send(200,"text/html",pag);
}


void gestorNoLocalizado(){
  String message = "Recurso No Localizado\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMetodo: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArgumentos: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
}



void webInit(){
  server.on("/",gestorRaiz);
  server.onNotFound(gestorNoLocalizado);
  server.begin();
  Serial.println("Servidor web ha sido iniciado");
}

void setup()
{
    Serial.begin(115200);
    delay(100);
    wifiInit();
    if(WiFi.status()==WL_CONNECTED)
        webInit();
}



void loop()
{
   server.handleClient();
   delay(2);
}
